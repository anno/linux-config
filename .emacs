(package-initialize)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (adwaita)))
 '(org-html-head
   "<link rel=\"stylesheet\" href=\"http://msnyder.info/theme/css/style.min.css\">")
 '(org-export-with-section-numbers nil)
 '(org-export-with-sub-superscripts (quote {}))
 '(org-startup-folded nil)
 '(org-use-sub-superscripts (quote {}))
 '(standard-indent 2)
 '(tab-always-indent t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(setenv "EDITOR" "emacsclient")
(setenv "PAGER" "cat")
(global-set-key "n" 'goto-line)

(defun my-revert-buffer ()
  (interactive)
  (revert-buffer t t))

(global-set-key "c" 'compile)
(global-set-key "g" 'my-revert-buffer)
(global-set-key "s" 'shell)
(global-set-key "f" 'find-file-at-point)
(put 'eval-expression 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-region 'disabled nil)

(setq text-mode-hook 
      '(lambda (&rest ignore)
	 (auto-fill-mode 1)			
	 (set-fill-column 75)))

(defun shell-current-directory ()
  (interactive)
  (let ((dir default-directory))
    (shell)
    (process-send-string (get-buffer-process (current-buffer))
			 (format "cd \"%s\"\n" (replace-regexp-in-string "~" (getenv "HOME") dir)))
    (setq default-directory dir))) 

(setq shell-mode-hook
      '(lambda (&rest ignore)
	 (global-set-key "s" 'shell-current-directory)
	 (local-set-key (kbd "M-p") 'comint-previous-matching-input-from-input)))

(add-hook 'dired-mode-hook '(lambda () (local-set-key (kbd "M-s") 'shell-current-directory)))

(defun find-uses ()
   (interactive)
  (save-excursion
    (backward-word 1)
    (and (looking-at "[A-Za-z][A-Za-z0-9_$]*")
	 (occur (concat "\\<" (match-string 0) "\\>")))))

(global-set-key "u" 'find-uses)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(ido-mode)
(server-start)