#!/bin/sh
# Installs Slitaz on a virtualbox disk. Creates a single, new partition

read -p 'Wipe out entire disk to install Slitaz (y/N) ' CHOOSE_INSTALL
[[ "$CHOOSE_INSTALL" != "y" ]] && echo "Aborting install" && exit 0
[[ $(id -u) != "0" ]] && echo "Must run as root" && exit 0

# Creates single partition with 32KB for MBR
parted -s /dev/sda <<EOF
mktable msdos
mkpart primary etx4 32KB -1
quit
EOF

cat > tazinst.rc <<EOF
MODE="install"
MEDIA="cdrom"
SOURCE=""
ROOT_UUID="/dev/sda1"
ROOT_FORMAT="ext4"
HOME_UUID=""
HOME_FORMAT=""
HOSTNAME="slitaz"
ROOT_PWD="root"
USER_LOGIN="tux"
USER_PWD="tux"
BOOTLOADER="auto"
WINBOOT=""
EOF

tazinst execute

eject /dev/cdrom

# Downloads postinstall script
mount /dev/sda1 /mnt
cd /mnt/root
wget http://anno-vbox.onlinewebshop.net/slitaz-postinstall.sh
cd
umount /mnt
