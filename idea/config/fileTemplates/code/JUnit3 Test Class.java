import junit.framework.TestCase;

/** Tests {@link ${NAME}}. */
public class ${NAME} extends TestCase {
  ${BODY}
}