#!/bin/sh
# Stage 1 after a fresh installation. Must be run as root.

[[ $(id -u) != "0" ]] && echo "Must run as root" && exit 0

# Start network explicitly and load the dynamic network module during subsequent boots:
repair_networking() {
  /etc/init.d/network.sh restart
  sed -i '/LOAD_MODULES/ s/""/"e1000"/' /etc/rcS.conf
}

# With RC2 the network configuration fails during boot.
# Test network and repair if needed.
ping -c 1 google.com > /dev/null 2>&1 || repair_networking

# Work around SliTaz certificate problems for git and wget
wget http://curl.haxx.se/ca/cacert.pem -O /etc/ssl/cert.pem

# Set /etc/TZ from Ubuntu's IP to time zone service.
wget -q -O - http://geoip.ubuntu.com/lookup | sed 's/</\n/g' | grep ^TimeZone | cut '-d>' -f2 > /etc/TZ

# In the US change the mirror.
grep ^America /etc/TZ > /dev/null && sed -i -e 's@mirror.slitaz.org@distro.ibiblio.org/slitaz@' /var/lib/tazpkg/mirror

# Configure for auto login.
echo "auto_login yes" >> /etc/slim.conf

BUSYBOX_INADEQUATE="curl wget tar dpkg"
# Get basic development goodies
for i in $BUSYBOX_INADEQUATE virtualbox-ose-guestutils emacs git rsync get-google-chrome
do tazpkg get-install $i
done

# Fix dependencies for google-chrome
sed -i -e 's/DEPENDS=.*$/DEPENDS="libexif GConf lzma bash libcups xdg-utils nss xorg-libXss xorg-libXtst alsa-lib xorg-libXi"/' /usr/bin/get-google-chrome

# Configure git to use certificates
mkdir /usr/etc 
git config --system http.sslCAinfo /etc/ssl/cert.pem

# Get font collection, unpack it as root, and get a matching fonts.config file
cd /usr/share/fonts
wget https://googledrive.com/host/0B7wUgu7vj0VSaXFzOURiLWg4cWM/fonts.zip
unzip fonts.zip
rm fonts.zip
curl -s -k -L http://goo.gl/uaQgb6  > /usr/share/fontconfig/conf.avail/52-local.conf
ln -s /usr/share/fontconfig/conf.avail/52-local.conf /etc/fonts/conf.d/

cd
wget -O .emacs https://googledrive.com/host/0B7wUgu7vj0VSaXFzOURiLWg4cWM/emacs.minimal

# Clean up
tazpkg clean-cache
echo "Current disk free report"
df -h
